package base

var (
	nilKind = NewKind("bitbucket.org/rj/goey/base.nil")
)

type nilElement struct{}

func (*nilElement) Close() {
	// No-op
}

func (*nilElement) Kind() *Kind {
	return &nilKind
}

func (*nilElement) Layout(bc Constraints) Size {
	if bc.IsBounded() {
		return bc.Max
	} else if bc.HasBoundedWidth() {
		return Size{bc.Max.Width, bc.Min.Height}
	} else if bc.HasBoundedHeight() {
		return Size{bc.Min.Width, bc.Max.Height}
	}
	return bc.Min
}

func (*nilElement) MinIntrinsicHeight(Length) Length {
	return 0
}

func (*nilElement) MinIntrinsicWidth(Length) Length {
	return 0
}

func (*nilElement) Props() Widget {
	return nil
}

func (*nilElement) SetBounds(Rectangle) {
	// Do nothing
}

func (*nilElement) UpdateProps(data Widget) error {
	panic("unreachable")
}
