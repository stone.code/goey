package base

// Mount will try to mount a widget.  In the case where the widget is non-nil,
// this function is a simple wrapper around calling the method Mount directly.
// If widget is nil, this function will instead return a non-nil element to act
// as a placeholder.
//
// The placeholder element has an intrinsic size of zero and adds no visible
// elements in the GUI.  Unlike other elements, there is no need to call Close,
// as that method is a no-op.
func Mount(parent Control, widget Widget) (Element, error) {
	if widget == nil {
		return (*nilElement)(nil), nil
	}
	return widget.Mount(parent)
}

// MountNil is a wrapper around Mount(parent,nil).
func MountNil() Element {
	return (*nilElement)(nil)
}

// MountSlice will try to mount all of the widgets in the slice.  If any of the
// widgets fails to mount, then the already created elements will be closed.
func MountSlice(parent Control, widgets []Widget) ([]Element, error) {
	// Pre-allocate a slice with enough capacity.
	children := make([]Element, 0, len(widgets))

	// Mount all of the children
	for _, v := range widgets {
		child, err := v.Mount(parent)
		if err != nil {
			CloseElements(children)
			return nil, err
		}

		children = append(children, child)
	}

	return children, nil
}
