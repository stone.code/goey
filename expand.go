package goey

import (
	"bitbucket.org/rj/goey/base"
)

var (
	expandKind = base.NewKind("bitbucket.org/rj/goey.Expand")
)

// Expand wraps another widget to indicate that the widget should expand to
// occupy any available space in a HBox or VBox.  When used in any other
// context, the widget will be ignored, and behavior delegated to the child
// widget.
//
// In an HBox or VBox, the widget will be positioned according to the rules
// of its child.  However, any excess space along the main axis will be added
// based on the ratio of the widget's factor to the sum of factors for all
// widgets in the box.
type Expand struct {
	Factor int         // Fraction (minus one) of available space used by this widget
	Child  base.Widget // Child widget.
}

// Kind returns the concrete type for use in the Widget interface.
// Users should not need to use this method directly.
func (*Expand) Kind() *base.Kind {
	return &expandKind
}

// Mount creates a button in the GUI.  The newly created widget
// will be a child of the widget specified by parent.
func (w *Expand) Mount(parent base.Control) (base.Element, error) {
	// Mount the child
	child, err := base.Mount(parent, w.Child)
	if err != nil {
		return nil, err
	}

	return &expandElement{
		parent:  parent,
		Element: child,
		factor:  w.Factor,
	}, nil
}

type expandElement struct {
	base.Element

	parent base.Control
	factor int
}

func (*expandElement) Kind() *base.Kind {
	return &expandKind
}

func (w *expandElement) updateProps(data *Expand) (err error) {
	w.Element, err = base.DiffChild(w.parent, w.Element, data.Child)
	w.factor = data.Factor

	return err
}

func (w *expandElement) UpdateProps(data base.Widget) error {
	return w.updateProps(data.(*Expand))
}
