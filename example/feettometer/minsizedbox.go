package main

import (
	"bitbucket.org/rj/goey/base"
)

var (
	minsizedboxKind = base.NewKind("bitbucket.org/rj/goey/example/feettometer.MinSizedBox")
)

// MinSizedBox is a custom layout widget that sizes its child widget according
// to the MinIntrinsicWidth and MinIntrinsicHeight of that widget.
type MinSizedBox struct {
	Child base.Widget // Child widget.
}

// Kind returns the concrete type for use in the.Widget interface.
// Users should not need to use this method directly.
func (*MinSizedBox) Kind() *base.Kind {
	return &minsizedboxKind
}

// Mount creates a this child in the GUI.  The newly created widget
// will be a child of the widget specified by parent.
func (w *MinSizedBox) Mount(parent base.Control) (base.Element, error) {
	// Mount the child
	child, err := base.Mount(parent, w.Child)
	if err != nil {
		return nil, err
	}

	return &minsizedboxElement{
		Element: child,
		parent:  parent,
	}, nil
}

type minsizedboxElement struct {
	base.Element

	parent    base.Control
	childSize base.Size
}

func (*minsizedboxElement) Kind() *base.Kind {
	return &minsizedboxKind
}

func (w *minsizedboxElement) Layout(bc base.Constraints) base.Size {
	width := w.Element.MinIntrinsicWidth(0)
	height := w.Element.MinIntrinsicHeight(width)

	size := bc.Constrain(base.Size{width, height})
	return w.Element.Layout(base.Tight(size))
}

func (w *minsizedboxElement) MinIntrinsicHeight(width base.Length) base.Length {
	return w.Element.MinIntrinsicHeight(width)
}

func (w *minsizedboxElement) MinIntrinsicWidth(height base.Length) base.Length {
	return w.Element.MinIntrinsicWidth(height)
}

func (w *minsizedboxElement) SetBounds(bounds base.Rectangle) {
	w.Element.SetBounds(bounds)
}

func (w *minsizedboxElement) updateProps(data *MinSizedBox) (err error) {
	w.Element, err = base.DiffChild(w.parent, w.Element, data.Child)
	w.childSize = base.Size{}

	return err
}

func (w *minsizedboxElement) UpdateProps(data base.Widget) error {
	return w.updateProps(data.(*MinSizedBox))
}
